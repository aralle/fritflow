package interfeis.com.fritflow;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;

import interfeis.com.fritflow.settings.GeneralSetting;
import okhttp3.OkHttpClient;
import okhttp3.Request;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inf = inflater.inflate(R.layout.fragment_home, container, false);

        RecyclerView rvProductList  = inf.findViewById(R.id.rvProductList );

        LinearLayoutManager llm     = new LinearLayoutManager( getActivity() );
        llm.setOrientation( RecyclerView.VERTICAL );

        GridLayoutManager glm       = new GridLayoutManager( getActivity(), 2 );

        rvProductList.setLayoutManager( llm );

        OkHttpClient okHC   = new OkHttpClient();
        Request okReq       = new Request.Builder()
                            .get()
                            .url(GeneralSetting.getUrlServer() + "")
                            .build();


        return inf;
    }

}
