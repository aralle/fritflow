package interfeis.com.fritflow;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import interfeis.com.fritflow.settings.GeneralSetting;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void btnLoginClicked(final View view){

        TextInputEditText txtUsername = (TextInputEditText) findViewById(R.id.txtUsername);
        TextInputEditText txtPassword = (TextInputEditText) findViewById(R.id.txtPassword);

        String strUsername = txtUsername.getText().toString();
        String strPassword = txtPassword.getText().toString();

        if(strUsername.length() == 0){

            txtUsername.setError(getString(R.string.email_required));

        }else if(Helper.isEmailValid(strUsername)==false){

            txtUsername.setError(getString(R.string.email_not_valid));

        }else if(strPassword.length() == 0){

            txtPassword.setError(getString(R.string.error_password_required));

        }else{

            OkHttpClient okHC       = new OkHttpClient();

            RequestBody okReqBody   = new MultipartBody.Builder()
                                        .setType( MultipartBody.FORM )
                                        .addFormDataPart( "username", strUsername )
                                        .addFormDataPart( "password", strPassword )
                                        .build();

            Request okReq           = new Request.Builder()
                                            .post( okReqBody )
                                            .url( GeneralSetting.getUrlServer() + "/wp-json/jwt-auth/v1/token" )
                                            .build();

            final ProgressDialog pd = new ProgressDialog( LoginActivity.this );

            pd.setTitle(getString(R.string.loading));
            pd.setMessage( getString(R.string.please_wait) );
            pd.setCancelable( false );

            pd.show();

            okHC.newCall( okReq ).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pd.dismiss();
                            Snackbar.make( view, getString(R.string.cannot_connect_server), Snackbar.LENGTH_LONG );
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    String respString   = response.body().string();

                    try {

                        JSONObject respJSON = new JSONObject( respString );

                        if( respJSON.has("token" ) ){

                            pd.dismiss();

                            String token        = respJSON.getString( "token" );
                            String userName     = respJSON.getString( "user_email" );
                            String niceName     = respJSON.getString( "user_nicename" );
                            String dispName     = respJSON.getString( "user_display_name" );

                            SharedPreferences.Editor dataApp   = getSharedPreferences( "DATA_APP", MODE_PRIVATE).edit();

                            dataApp.putString( "token", token );
                            dataApp.putString( "user_email", userName );
                            dataApp.putString( "user_nicename", niceName );
                            dataApp.putString( "user_display_name", dispName );

                            Intent i = new Intent( getApplicationContext(), MainActivity.class );

                            startActivity( i );
                            finish();

                        }else{

                            final String message      = respJSON.getString( "message" );

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    pd.dismiss();
                                    Snackbar.make( view, message, Snackbar.LENGTH_LONG );

                                }
                            });

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            });

            SharedPreferences.Editor data_app = getSharedPreferences("DATA_APP", MODE_PRIVATE).edit();

            data_app.putString("data_email", strUsername);
            data_app.commit();

            Intent i = new Intent(getApplicationContext(), MainActivity.class);

            startActivity(i);
        }
    }

    public void tvSignupClicked(View view) {
        Intent i = new Intent(getApplicationContext(), RegisterActivity.class);

        startActivity(i);
    }

    public void tvForgotPasswordClicked(View view) {

        Intent i = new Intent(getApplicationContext(), ForgotPasswordActivity.class);

        startActivity(i);
    }
}
