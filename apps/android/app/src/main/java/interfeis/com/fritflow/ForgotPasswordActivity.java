package interfeis.com.fritflow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.textfield.TextInputEditText;

public class ForgotPasswordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
    }

    public void btnSubmitClicked(View view) {

        TextInputEditText txtUsername = (TextInputEditText) findViewById(R.id.txtUsername);

        String strUsername = txtUsername.getText().toString();

        if(strUsername.length() == 0){

            txtUsername.setError(getString(R.string.email_required));

        }else if(Helper.isEmailValid(strUsername)==false){

            txtUsername.setError(getString(R.string.email_not_valid));

        }else{

            Intent i = new Intent(getApplicationContext(), MainActivity.class);

            startActivity(i);
        }
    }

    public void tvLoginClicked(View view) {

        Intent i = new Intent(getApplicationContext(), LoginActivity.class);

        startActivity(i);
    }
}
