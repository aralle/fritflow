<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'fritflow' );

/** MySQL database username */
define( 'DB_USER', 'wp' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wp' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'sE1]F4fM0bYgx-fa4(2b%=v<`{I#32s9cl `I(Jpq=^(#EQxv.(kl>MdFJh._@{j' );
define( 'SECURE_AUTH_KEY',   'yop`C{3t/n#IrzAcB0XznSv}#Ye_MKwG2uKdnqG-d6skU08f}NA &g^U#zFayy5}' );
define( 'LOGGED_IN_KEY',     '+/Zlg7Ue{%[vtsKEHH!*MF{6yHS#Zj!*G/NlE_^7Db$ul6XgA:A/YDmwjNk83%av' );
define( 'NONCE_KEY',         'P2FO`)WsWLrjD<[v`@w4xAh=oduIJ8L7u!K/sL0(!jh)LxucLS8r+&!S}wP6oSbi' );
define( 'AUTH_SALT',         ':TL70PJfy;6xc-N;IgoxYNFB(M,yQL8cx/6SE?f!6e8,lDZ(}NbqUM+S,E?AXjt{' );
define( 'SECURE_AUTH_SALT',  'Q;f2HS_8U6LBqDP&O#O@ 5,:=/8CN&iVK ^UbTMD|AjO=V1F-4<}>hzEm9e(,fdo' );
define( 'LOGGED_IN_SALT',    '.G:ICxQaI|IU<Uy5TEMnk>(.(AdA`l%`wtO>3z+W(~7CvZ0>,d~gSYPc]_i_3$ig' );
define( 'NONCE_SALT',        'ebp&E]1>_TNrf3=g}{Xr<PEx(}0cwYg#zoW@iDgxS50N>1@BTR3rT%c+`pR;$FUA' );
define( 'WP_CACHE_KEY_SALT', 'v86-%{}.DpI68c9Kajs.Iv&2BER^$rZ~IEIX);N-3L<!A{PDd,%TsJr5M:8!:~T]' );
define('JWT_AUTH_SECRET_KEY', 'u.|ucCH#||HN6N0Dv1%DxZ?1Lj>;3#njbDTzfBjj#@+0(5^G9 H7&-;e7D/5a!%J');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


define( 'WP_DEBUG', true );
define( 'SCRIPT_DEBUG', true );

define('JWT_AUTH_CORS_ENABLE', true);
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
