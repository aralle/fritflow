package interfeis.com.fritflow;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordFragment extends Fragment {

    TextInputEditText txtOldPassword;
    TextInputEditText txtNewPassword;
    TextInputEditText txtConfPass;

    Button btnChangePass;

    public ChangePasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View thefragment = inflater.inflate(R.layout.fragment_change_password, container, false);

        txtOldPassword = (TextInputEditText) thefragment.findViewById(R.id.txtOldPassword);
        txtNewPassword = (TextInputEditText) thefragment.findViewById(R.id.txtNewPassword);
        txtConfPass    = (TextInputEditText) thefragment.findViewById(R.id.txtConfPassword);

        btnChangePass = (Button) thefragment.findViewById(R.id.btnChangePass);

        btnChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSubmitButtonClicked(view);
            }
        });

        // Inflate the layout for this fragment
        return thefragment;
    }

    public void onSubmitButtonClicked(View view){

        String strOldPassword = txtOldPassword.getText().toString();
        String strNewPassword = txtNewPassword.getText().toString();
        String strConfPassword = txtConfPass.getText().toString();

        if(strNewPassword.length()==0){

            txtNewPassword.setError(getString(R.string.error_password_required));

        }else if(strNewPassword.equals(strConfPassword)==false){

            txtConfPass.setError(getString(R.string.conf_password_must_same));

        }else{

            Toast.makeText( getActivity(), getString(R.string.success), Toast.LENGTH_SHORT).show();
            Snackbar.make(view, R.string.success, Snackbar.LENGTH_SHORT).show();
        }

    }

}
