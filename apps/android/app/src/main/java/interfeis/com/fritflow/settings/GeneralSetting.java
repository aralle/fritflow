package interfeis.com.fritflow.settings;

public class GeneralSetting {

    public static String URL_SERVER     = "http://fritflow.com/";
    public static String JWT_KEY        = "testing";

    public static String getUrlServer(){

        return URL_SERVER;
    }

    public static String getJWTKey(){

        return JWT_KEY;

    }
}
