package interfeis.com.fritflow;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.google.zxing.integration.android.IntentIntegrator;

public class MainActivity extends AppCompatActivity {

    ActionBarDrawerToggle left_menu_toggle;

    boolean clickExit = false;

    SharedPreferences spData;

    @Override
    public void onBackPressed() {

        if(clickExit==true){
            super.onBackPressed();
            finishAffinity();
            return;
        }
        clickExit = true;

        Toast.makeText(getApplicationContext(), getString(R.string.press_again_to_exit), Toast.LENGTH_LONG).show();

        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                clickExit = false;
            }
        }, 3000);

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        left_menu_toggle.syncState();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        openAppFragment(new HomeFragment());

        spData = getSharedPreferences("DATA_APP", MODE_PRIVATE);

        final DrawerLayout main_act_layout = (DrawerLayout) findViewById(R.id.main_activity_layout);

        NavigationView left_menu = (NavigationView) findViewById(R.id.left_menu);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        left_menu_toggle = new ActionBarDrawerToggle( MainActivity.this,
                main_act_layout,
                (R.string.open),
                (R.string.close)
        );

        main_act_layout.addDrawerListener(left_menu_toggle);

        left_menu.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                if(menuItem.getItemId() == R.id.left_menu_home){

                    openAppFragment(new HomeFragment());

                    Toast.makeText(getApplicationContext(), getString(R.string.home), Toast.LENGTH_SHORT).show();
                    main_act_layout.closeDrawers();

                }else if(menuItem.getItemId() == R.id.left_menu_change_password){

                    openAppFragment(new ChangePasswordFragment());

                    Toast.makeText(getApplicationContext(), getString(R.string.change_password), Toast.LENGTH_SHORT).show();
                    main_act_layout.closeDrawers();

                }else if(menuItem.getItemId() == R.id.left_menu_logout){

                    removeSharedData();

                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(i);

                    finish();
                }else if(menuItem.getItemId() == R.id.left_menu_exit){

                    removeSharedData();
                    finishAffinity();
                }

                return false;
            }
        });
    }

    public void openAppFragment(Fragment f){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_main, f)
                .commit();
    }

    public void removeSharedData(){
        getSharedPreferences("DATA_APP", MODE_PRIVATE).edit().clear().commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        left_menu_toggle.onOptionsItemSelected(item);

        return super.onOptionsItemSelected(item);
    }
}
