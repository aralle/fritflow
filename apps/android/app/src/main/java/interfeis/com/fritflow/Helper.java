package interfeis.com.fritflow;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Helper {

    public static boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
