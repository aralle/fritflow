package interfeis.com.fritflow;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import interfeis.com.fritflow.settings.GeneralSetting;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegisterActivity extends AppCompatActivity {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    public void btnSignupClicked(final View view) {

        TextInputEditText txtFullname = (TextInputEditText) findViewById(R.id.txtFullname);
        TextInputEditText txtUsername = (TextInputEditText) findViewById(R.id.txtUsername);
        TextInputEditText txtPassword = (TextInputEditText) findViewById(R.id.txtPassword);

        String strFullname = txtFullname.getText().toString();
        String strUsername = txtUsername.getText().toString();
        String strPassword = txtPassword.getText().toString();

        if(strFullname.length() == 0){

            txtFullname.setError(getString(R.string.full_name_required));

        }else if(strFullname.length() <= 2){

            txtFullname.setError(getString(R.string.full_name_must_more_2_character));

        }else if(strUsername.length() == 0){

            txtUsername.setError(getString(R.string.email_required));

        }else if(Helper.isEmailValid(strUsername)==false){

            txtUsername.setError(getString(R.string.email_not_valid));

        }else if(strPassword.length() == 0){

            txtPassword.setError(getString(R.string.error_password_required));

        }else{

            String[] arrName        = strFullname.split("\\s+" );

            String strFirstName     = arrName[0];
            String strLastName      = "";

            for( int i=1; i < arrName.length; i++ ){

                strLastName     = strLastName + arrName[i];

            }

            String jsonInfoString   = "";
            JSONObject jsonUserInfo = new JSONObject();
            try {

                jsonUserInfo.put( "username", strUsername );
                jsonUserInfo.put( "email", strUsername );
                jsonUserInfo.put( "password", strPassword );

                jsonInfoString          = jsonUserInfo.toString();

            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient okHC       = new OkHttpClient();

            RequestBody okReqJSON   = RequestBody.create( this.JSON, jsonInfoString );

            Request okReq           = new Request.Builder()
                    .post( okReqJSON )
                    .url(GeneralSetting.getUrlServer() + "/wp-json/wp/v2/users/register")
                    .build();

            final ProgressDialog pd = new ProgressDialog( RegisterActivity.this );
            pd.setTitle( getString( R.string.loading ) );
            pd.setMessage( getString( R.string.please_wait ) );
            pd.setCancelable( false );

            pd.show();

            okHC.newCall( okReq ).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            pd.dismiss();
                            Snackbar.make( view, getString( R.string.cannot_connect_server ), Snackbar.LENGTH_LONG );
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    String respString   = response.body().string();

                    try {

                        JSONObject respJson = new JSONObject( respString );

                        String respCode     = respJson.getString("code" );
                        final String respMsg      = respJson.getString( "message" );
                        Log.e("message", respMsg );
                        if( respCode == "200" ){

                            pd.dismiss();
                            Snackbar.make( view, getString(R.string.email_been_sent), Snackbar.LENGTH_LONG).show();

                            Handler h = new Handler();
                            h.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);

                                    startActivity(i);
                                }
                            },2500);

                        }else{

                            pd.dismiss();
                            Snackbar.make( view, respMsg, Snackbar.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

        }
    }

    public void tvLoginClicked(View view) {
        Intent i = new Intent(getApplicationContext(), LoginActivity.class);

        startActivity(i);
    }
}
